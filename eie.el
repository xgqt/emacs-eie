;;; eie.el --- Run Emacs inside Emacs -*- lexical-binding: t -*-


;; This file is part of eie - run Emacs inside Emacs.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; eie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; eie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with eie.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 21 Jul 2022
;; Version: 1.0.0
;; Keywords: lisp maint tools
;; Homepage: https://gitlab.com/xgqt/emacs-eie/
;; Package-Requires: ((emacs "24.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Run a Emacs batch process REPL inside Emacs.



;;; Code:


(require 'comint)


(defconst eie-version "1.0.0"
  "EiE package version.")


(defgroup eie nil
  "Run a Emacs batch process REPL inside Emacs."
  :group 'emacs
  :group 'external)


;; TODO: Is there a way to get path to Emacs from itself?

(defcustom eie-emacs-executable (executable-find "emacs")
  "Path the Emacs executable used by `eie'."
  :safe 'stringp
  :type 'file
  :group 'eie)

(defcustom eie-extra-emacs-flags '("-Q")
  "Path the Emacs executable used by `eie'."
  :type '(repeat string)
  :group 'eie)

(defconst eie--load-file-name load-file-name
  "Path to the `eie' main library file.")

(defconst eie--load-directory-name (file-name-directory eie--load-file-name)
  "Path to the `eie' library directory.")


;; TODO: Quit on C-d, multi-line.

(defun eie--eval-function ()
  "Function that is ran in new Emacs process."
  (defalias 'exit #'kill-emacs)  ;; TODO: Do not do this globally.
  (message ";; Welcome to EiE")
  (let ((input-counter 0))
    (while t
      (let* ((input (read-string "(EIE)> "))
             (input-result
              (ignore-errors
                (eval (car (read-from-string input))))))
        (message (format "$%d = %s" input-counter input-result))
        (setq input-counter (+ input-counter 1))))))

(defun eie--make-flags ()
  "Create a list of flags for `eie'."
  `(,@eie-extra-emacs-flags
    "--batch"
    "-L" ,eie--load-directory-name
    "--load" ,eie--load-file-name
    "--eval" "(eie--eval-function)"))

(defun eie--make-comint-in-buffer (buffer)
  "Run a `eie' REPL in a BUFFER."
  (apply #'make-comint-in-buffer
         "EiE"
         buffer
         eie-emacs-executable
         nil
         (eie--make-flags)))


;;;###autoload
(defun eie ()
  "Run a Emacs batch process REPL inside Emacs.
This is the main entry point to spawn a new `eie' REPL."
  (interactive)
  (let ((buffer (get-buffer-create "*EiE*"))
        (process (comint-check-proc "*EiE*")))
    (unless process
      (with-current-buffer buffer
        (eie--make-comint-in-buffer buffer)))
    (switch-to-buffer buffer)))


(provide 'eie)



;;; eie.el ends here
