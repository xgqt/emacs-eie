# EiE

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/emacs-eie">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/emacs-eie/">
    </a>
    <a href="https://gitlab.com/xgqt/emacs-eie/pipelines">
        <img src="https://gitlab.com/xgqt/emacs-eie/badges/master/pipeline.svg">
    </a>
</p>

Run Emacs inside Emacs.


## About

Run a Emacs batch process REPL inside Emacs.


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
